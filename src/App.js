import logo from "./logo.svg";
import "./App.css";
import Heading from "./components/Heading";
import Names from "./components/Names";
import PerPersonInfo from "./components/PerPersonInfo";

function App() {
  return (
    <div className="App">
      <Heading />
      <Names />
      <PerPersonInfo />
    </div>
  );
}

export default App;
