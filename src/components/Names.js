import got from "../data";
import Families from "./Families";
import "./Names.css";

function Names() {
  return (
    <div className="family-div">
      {got.houses.map((element) => {
        return <Families name={element.name} />;
      })}
    </div>
  );
}

export default Names;
