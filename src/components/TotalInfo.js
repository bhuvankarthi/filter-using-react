function TotalInfo(props) {
  //   console.log(props);
  const image = props.name.image;
  const nameOfAPerson = props.name.name;
  const description = props.name.description;
  const anchor = props.name.wikiLink;
  return (
    <div>
      <img src={image} alt={nameOfAPerson + "image"}></img>
      <h2>{nameOfAPerson}</h2>
      <p>{description}</p>
      <button className="know-more">
        <a href={anchor}>Know More!</a>
      </button>
    </div>
  );
}

export default TotalInfo;
