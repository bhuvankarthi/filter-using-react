import TotalInfo from "./TotalInfo";
import got from "../data";
import "./PerPersonInfo.css";

function PerPersonInfo() {
  return (
    <div className="person-div">
      {got.houses.map((element) =>
        element.people.map((person) => <TotalInfo name={person} />)
      )}
    </div>
  );
}

export default PerPersonInfo;
